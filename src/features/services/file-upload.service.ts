import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';



@Injectable()

export class FileUploadService {

  constructor(private http: HttpClient) { }

  addFile(profileImage: File,origin_file:string): Observable<any> {
    var formData: any = new FormData();
    formData.append("file", profileImage);

    let api = origin_file == "docx"? 'word2pdf':'pdf2word'

    return this.http.post(`${environment.BASE_URL}/${api}`, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(
      catchError(this.errorMgmt)
    )
  }

  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
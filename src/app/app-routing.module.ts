import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: 'convertion', loadChildren: () => import('../pages/convertion/convertion.module').then(m => m.ConvertionModule) },
{
  path:"",
  redirectTo:"convertion",
  pathMatch:'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

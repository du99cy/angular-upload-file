import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConvertionRoutingModule } from './convertion-routing.module';
import { ConvertionComponent } from './convertion.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';

const COMMON_MODULES = [FormsModule,ReactiveFormsModule,HttpClientModule]
const MATERIAL_MODULES =[MatButtonModule]
const COMPONENTS = [ConvertionComponent]


@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    CommonModule,
    ConvertionRoutingModule,
    ...COMMON_MODULES,
    ...MATERIAL_MODULES
  ]
})
export class ConvertionModule { }

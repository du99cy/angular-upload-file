import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConvertionComponent } from './convertion.component';

const routes: Routes = [
  {
    path: '',
    component: ConvertionComponent,
    pathMatch: 'full'
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConvertionRoutingModule { }

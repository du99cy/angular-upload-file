import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FileUploadService } from '@features/services/file-upload.service';

@Component({
  selector: 'app-convertion',
  templateUrl: './convertion.component.html',
  styleUrls: ['./convertion.component.scss'],
  providers:[FileUploadService]
})
export class ConvertionComponent implements OnInit {
  origin_file = 'docx'
  new_file = 'pdf'

  link_download = ""

  form: FormGroup;
  progress: number = 0;

  constructor(
    public fb: FormBuilder,
    public fileUploadService: FileUploadService
  ) {
    this.form = this.fb.group({
      file: [null]
    })
  }

  ngOnInit() { }

  uploadFile(event:any) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      file: file,
    });
    
    this.form.get('file').updateValueAndValidity()
  }

  ConvertFile() {
    this.fileUploadService.addFile(
      this.form.value.file,this.origin_file
    ).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          console.log('Request has been made!');
          break;
        case HttpEventType.ResponseHeader:
          console.log('Response header has been received!');
          break;
        case HttpEventType.UploadProgress:
          this.progress = Math.round(event.loaded / (event as any).total * 100);
          console.log(`Uploaded! ${this.progress}%`);
          break;
        case HttpEventType.Response:
          this.link_download = event.body.link_download
          setTimeout(() => {
            this.progress = 0;
          }, 1500);

      }
    })
  }

  changeFormat()
  {
    [this.origin_file,this.new_file] = [this.new_file,this.origin_file]
    this.link_download = ''
  }

}
